﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public static class FileDownloader
{
    private static string url = "http://localhost/unity/";
    private static Vector2 location = new Vector2(-1,-1);

    public static IEnumerator DownloadFile(string path) {
        using (UnityWebRequest www = UnityWebRequest.Get(url+path)) {
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError) {
                //Debug.Log("File not found");
            }
            else {
                Debug.Log("Download " + path);
                System.IO.File.WriteAllBytes(Application.persistentDataPath + "/" + path, www.downloadHandler.data);
            }
        }
    }

    public static IEnumerator DownloadDatabase(Action callback = null) {
        yield return GetLocation();
        Debug.Log(url + "database.json?latitude=" + location.x + "&longitude=" + location.y);
        using (UnityWebRequest www = UnityWebRequest.Get(url + "database.json?latitude=" + location.x + "&longitude=" + location.y)) {
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError) {
                Debug.Log("Error");
            }
            else {
                // Copy Database
                Debug.Log("Copy Database");
                TextAsset dbText = new TextAsset(www.downloadHandler.text);
                System.IO.File.WriteAllBytes(Application.persistentDataPath + "/database.json", dbText.bytes);
                foreach (string imageName in JsonUtility.FromJson<ImageDatabase>(www.downloadHandler.text).ImageList) {
                    yield return DownloadFile("targets/" + imageName + ".jpg");
                    yield return DownloadFile("data/" + imageName + ".json");
                    yield return DownloadFile("data/" + imageName + ".png");
                }
                if (callback != null)
                callback();
            }
        }
    }

    public static IEnumerator GetLocation() {
        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
            yield break;

        // Start service before querying location
        Input.location.Start();

        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0) {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1) {
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed) {
            //print("Unable to determine device location");
            yield break;
        }
        else {
            // Access granted and location value could be retrieved
            location.x = Input.location.lastData.latitude;
            location.y = Input.location.lastData.longitude;
        }

        // Stop service if there is no need to query location updates continuously
        Input.location.Stop();
    }
}
