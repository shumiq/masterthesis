﻿using EasyAR;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class ImageTargetListManager : MonoBehaviour {

    [SerializeField] ImageTrackerBehaviour imageTracker;
    [SerializeField] TableManager table;

    void Start() {

        // Read Database to get images list
        FileManager.DBRead();

        // Create ImageTarget
        foreach(string imageName in FileManager.imageDB.Data.Keys) {
            CreateImageTarget(imageName);
        }
    }

    private void CreateImageTarget(string imageName) {
        GameObject imageTargetInstance = (GameObject)Instantiate(Resources.Load("Prefabs/Target"));
        imageTargetInstance.transform.SetParent(this.transform);
        imageTargetInstance.transform.localPosition = Vector3.zero;
        imageTargetInstance.transform.localScale = Vector3.one;
        ImageTargetBehaviour behaviour = imageTargetInstance.GetComponent<ImageTargetBehaviour>();
        behaviour.Bind(imageTracker);
        behaviour.SetupWithImage(FileManager.imageDB.Data[imageName].ImagePath, StorageType.Absolute, imageName, new Vector2(100, 100));
        TargetManager targetManager = imageTargetInstance.GetComponent<TargetManager>();
        targetManager.ImageTarget = behaviour;
        targetManager.TargetList = this;

        if(FileManager.imageDB.Data[imageName].Type == "Image") {
            SpriteRenderer imageData = imageTargetInstance.GetComponentInChildren<SpriteRenderer>();
            imageData.sprite = FileManager.imageDB.Data[imageName].Image;
            float scale = 1f / imageData.size.x;
            imageData.gameObject.transform.localScale = new Vector3(scale, scale, 1f);
        }
    }

    public void ShowTable(string name) {
        table.ShowTable(name);
    }

    public void HideTable() {
        table.HideTable();
    }
}
