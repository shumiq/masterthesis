﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class InitializeApp : MonoBehaviour
{
    [SerializeField] private GameObject main;
    void Start()
    {
        // Create directories if not exists
        if (!Directory.Exists(Application.persistentDataPath + "/targets")) {
            Directory.CreateDirectory(Application.persistentDataPath + "/targets");
        }
        if (!Directory.Exists(Application.persistentDataPath + "/data")) {
            Directory.CreateDirectory(Application.persistentDataPath + "/data");
        }
        // Copy Database if not exists or have changes
        if (!File.Exists(Application.persistentDataPath + "/database.json") ||
            Resources.Load<TextAsset>("default/database").text != FileManager.ReadTextFile(Application.persistentDataPath + "/database.json")) {
            StartCoroutine(FileDownloader.DownloadDatabase(() => {
                main.SetActive(true);
            }));
        }
        else {
            main.SetActive(true);
        }

    }

    private void CopyFromResource() {
        Debug.Log("Copy database...");
        // Copy Database
        TextAsset dbText = Resources.Load<TextAsset>("default/database");
        System.IO.File.WriteAllBytes(Application.persistentDataPath + "/database.json", dbText.bytes);
        // Copy Images
        Texture2D[] targets = Resources.LoadAll<Texture2D>("default/targets");
        foreach (Texture2D target in targets) {
            System.IO.File.WriteAllBytes(Application.persistentDataPath + "/targets/" + target.name + ".jpg", target.EncodeToJPG());
        }
        // Copy Data
        TextAsset[] dataFiles = Resources.LoadAll<TextAsset>("default/data");
        foreach (TextAsset data in dataFiles) {
            System.IO.File.WriteAllBytes(Application.persistentDataPath + "/data/" + data.name + ".json", data.bytes);
        }
        Texture2D[] imageDataFiles = Resources.LoadAll<Texture2D>("default/data");
        foreach (Texture2D imageData in imageDataFiles) {
            System.IO.File.WriteAllBytes(Application.persistentDataPath + "/data/" + imageData.name + ".png", imageData.EncodeToPNG());
        }
    }
}
