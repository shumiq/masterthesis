﻿using EasyAR;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetManager : MonoBehaviour
{
    public ImageTargetBehaviour ImageTarget;
    public ImageTargetListManager TargetList;

    private void OnEnable() {
        if (ImageTarget == null || TargetList == null) return;
        if (ImageTarget.Name == "") return;
        if (FileManager.imageDB.Data[ImageTarget.Name].Type != "Table") return;
        TargetList.ShowTable(ImageTarget.Name);
    }

    private void OnDisable() {
        if (ImageTarget == null || TargetList == null) return;
        if (ImageTarget.Name == "") return;
        if (FileManager.imageDB.Data[ImageTarget.Name].Type != "Table") return;
        TargetList.HideTable();
    }
}
