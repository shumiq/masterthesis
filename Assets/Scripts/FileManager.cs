﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class FileManager
{
    public static ImageDatabase imageDB;

    public static void DBRead() {
        string path = GetPath("database.json");
        imageDB = JsonUtility.FromJson<ImageDatabase>(ReadTextFile(path));
        imageDB.Data = new Dictionary<string, DataDatabase>();
        foreach (string imageName in imageDB.ImageList) {
            string imagePath = FileManager.GetPath("targets/" + imageName + ".jpg");
            string dataPath = FileManager.GetPath("data/" + imageName + ".json");
            if (imagePath != null && dataPath != null) {
                // Data is json
                // Read data json
                DataDatabase data = JsonUtility.FromJson<DataDatabase>(ReadTextFile(dataPath));
                // Add image path to data
                data.ImagePath = imagePath;
                // Type is Table
                data.Type = "Table";
                // Add data to imageDB
                imageDB.Data.Add(imageName, data);
            } else if((dataPath = FileManager.GetPath("data/" + imageName + ".png"))!=null) {
                // Data is Image
                // Create a new data object
                DataDatabase data = new DataDatabase();
                data.Title = imageName;
                data.ImagePath = imagePath;
                // Assign Image
                data.Image = LoadSprite(dataPath);
                // Type is Image
                data.Type = "Image";
                // Add data to imageDB
                imageDB.Data.Add(imageName, data);
            }
        }
    }

    public static string GetPath(string file) {
        if (File.Exists(Path.Combine(Application.persistentDataPath,file))) {
            return Path.Combine(Application.persistentDataPath, file);
        }
        return null;
    }

    public static string ReadTextFile(string path) {
        var sr = new StreamReader(path);
        var fileContents = sr.ReadToEnd();
        sr.Close();
        return fileContents;
    }

    private static Sprite LoadSprite(string path) {
        if (string.IsNullOrEmpty(path)) return null;
        if (System.IO.File.Exists(path)) {
            byte[] bytes = System.IO.File.ReadAllBytes(path);
            Texture2D texture = new Texture2D(1, 1);
            texture.LoadImage(bytes);
            Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            return sprite;
        }
        return null;
    }
}
