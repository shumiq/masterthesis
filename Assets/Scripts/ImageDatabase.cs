﻿using System.Collections.Generic;

[System.Serializable]
public class ImageDatabase
{
    public string[] ImageList;
    public Dictionary<string, DataDatabase> Data;
}
