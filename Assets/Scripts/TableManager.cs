﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class TableManager : MonoBehaviour
{
    [SerializeField] Text title;
    [SerializeField] GridLayoutGroup layout;
    [SerializeField] int width;
    [SerializeField] int height;
    [SerializeField] Button closeButton;

    void Start() {
        closeButton.onClick.AddListener(() => {
            this.gameObject.SetActive(false);
        });
    }

    private void addCell(string text) {
        GameObject cellInstance = (GameObject)Instantiate(Resources.Load("Prefabs/Cell"));
        cellInstance.transform.SetParent(layout.transform);
        cellInstance.transform.localPosition = Vector3.zero;
        cellInstance.transform.localScale = Vector3.one;
        cellInstance.transform.localRotation = new Quaternion(0, 0, 0, 0);
        Text cellText = cellInstance.GetComponent<Text>();
        cellText.supportRichText = true;
        cellText.text = text;
    }

    private void createTable(string name) {
        DataDatabase data = FileManager.imageDB.Data[name];
        title.supportRichText = true;
        title.text = "<b>"+ data.Title + "</b>\n" + data.Caption;

        // Remove old data
        foreach (Transform child in layout.transform) {
            GameObject.Destroy(child.gameObject);
        }

        if (data.Data.Length > 0) {
            layout.cellSize = new Vector2(width / data.NumCol, height / data.NumRow);
            for (int i = 0; i < data.NumRow; i++) {
                for (int j = 0; j < data.NumCol; j++) {
                    string text = data.GetData(i, j);
                    if ((data.IsHeaderCol && i == 0) || (data.IsHeaderRow && j == 0))
                        text = "<b>" + text + "</b>";
                    addCell(text);
                }
            }
            this.gameObject.SetActive(true);
        }
        else if(data.OSMNode.Length>0) {
            this.gameObject.SetActive(true);
            StartCoroutine(GetNodeInfo(data.OSMNode));
        }
        else {
            layout.cellSize = new Vector2(width, height);
            addCell("No Data");
        }
    }

    public void ShowTable(string name) {
        if (name == "") return;
        StopCoroutine("DelayHide");
        createTable(name);
    }

    public void HideTable() {
        if (name == "" || !this.gameObject.activeInHierarchy) return;
        StartCoroutine("DelayHide");
    }

    IEnumerator DelayHide() {
        yield return new WaitForSeconds(5);
        this.gameObject.SetActive(false);
    }
    
    IEnumerator GetNodeInfo(string node, string type = "node") {
        string osmApi = "https://www.openstreetmap.org/api/0.6/" + type + "/";
        using (UnityWebRequest www = UnityWebRequest.Get(osmApi+node)) {
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError) {
                if(type=="node")
                    StartCoroutine(GetNodeInfo(node, "way"));
                else
                    Debug.Log(www.error);
            }
            else {
                //Debug.Log(www.downloadHandler.text);
                string[] lines = www.downloadHandler.text.Split('\n');
                int count = 0;
                foreach (string line in lines) {
                    if (!line.Contains("<tag k=\"")) continue;
                    string[] words = line.Split('"');
                    addCell("<b>" + words[1] + "</b>");
                    addCell(words[3]);
                    count++;
                }
                layout.cellSize = new Vector2(width / 2, height / count);
            }
        }
    }
}
