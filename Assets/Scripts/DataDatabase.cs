﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataDatabase {
    public string Title;
    public string ImagePath;
    public string Type;
    // Type = Table
    public string Caption;
    public bool IsHeaderCol;
    public bool IsHeaderRow;
    public int NumCol;
    public int NumRow;
    public string[] Data;
    public string GetData(int row, int col) {
        return Data[NumCol * row + col];
    }

    // Type = OSM
    public string OSMNode;

    // Type = Image
    public Sprite Image;
}
